import url from './Url';
import EncryptedStorage from 'react-native-encrypted-storage';
import jwt_decode from "jwt-decode";

const axios = require('axios').default;

const serverUrl = `${url}/api`



export const decodeDealer_jwt = async () => {
    try {
        const token = await EncryptedStorage.getItem("dealer-token");
        if (token) {
            let decoded = jwt_decode(token);
            return decoded
        }
        else {
            return null
        }
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}
export const login = async (dealerObj) => {
    try {
        const { data: res } = await axios.post(`${serverUrl}/login`, dealerObj);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}

export const RegisterDealer = async (dealerObj) => {
    try {
        const { data: res } = await axios.post(`${serverUrl}/register`, dealerObj);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}

export const getCategory = async () => {
    try {
        const { data: res } = await axios.get(`${serverUrl}/category`);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}

export const getCategoryDetails = async (id) => {
    try {
        const { data: res } = await axios.get(`${serverUrl}/category/${id}`);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}

export const getProducts = async () => {
    try {
        const { data: res } = await axios.get(`${serverUrl}/products`);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}

export const getProductDetails = async (id) => {
    try {
        const { data: res } = await axios.get(`${serverUrl}/product/${id}`);
        return res;
    }
    catch (err) {
        console.log(err);
        return err.message
    }
}
