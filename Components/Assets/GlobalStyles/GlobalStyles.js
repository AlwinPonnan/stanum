'use strict';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const Globalstyles = StyleSheet.create({



    /////////////////////Design Related Styles (Image dimensions , borderRadius, positions etc)
    gradientSection: {
        height: hp(35),
        width: wp(100),
        zIndex: 0,
        position: "relative"
    },
    gradientSectionWithRadius: {
        height: hp(35),
        width: wp(100),
        position: "relative",
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
    },
    logo: { position: "absolute", top: 60, height: 150, width: "100%", alignSelf: "center" },








    //////////////////Basic Element styles (textInput , checkbox, labels, buttons etc) 
    TextInputContainer: {
        width: wp(90),
        justifyContent: "center",
        alignSelf: "center",
    },
    textInputLabel: {
        fontSize: 16,
        fontWeight: "500",
        paddingLeft: 30,
        marginVertical: 5
    },
    textInputs: {
        width: wp(90),
        shadowColor: "#000",
        paddingHorizontal: 20,
        borderColor: "transparent",
        borderWidth: 1,
        color: "black",
        backgroundColor: "white",
        marginBottom: 20,
        borderRadius: 20,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 0,

        elevation: 5,
        alignSelf: "center"
    },
    SearchInput: {
        width: wp(90),
        shadowColor: "#000",
        color: "black",
        backgroundColor: "white",
        paddingHorizontal: 20,
        borderColor: "transparent",
        borderWidth: 1,
        position: "absolute",
        bottom: -40,
        marginBottom: 20,
        zIndex: 2,
        borderRadius: 30,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,

        elevation: 5,
        alignSelf: "center"
    },
    yellowButton: {
        backgroundColor: "#F6C332",
        borderRadius: 30,
        alignSelf: "center",
        paddingVertical: 6,
        paddingHorizontal: 20
    },
    buttonText: {
        alignSelf: "center",
        fontWeight: "700"
    },
    underlinedTxt: {
        textDecorationLine: "underline",
        textDecorationStyle: "dotted",
        textShadowColor: "grey",
        fontStyle: "italic",
    },
    whiteButton: {
        backgroundColor: "#fff",
        borderRadius: 8,
        alignSelf: "center",
        paddingVertical: 8,
        paddingHorizontal: 20
    },
    whiteButtonText: {
        alignSelf: "center",
    },


    //////////////////////////////flex Styles which are commonly used 
    flexRow: {
        display: 'flex',
        flexDirection: 'row',
    },
    flexColumn: {
        display: 'flex',
        flexDirection: 'column',
    },
    flexRowJustifyCenter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    flexRowJustifyBetween: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    flexRowJustifyAround: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    flexColumnJustifyCenter: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    flexColumnJustifyBetween: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    flexColumnJustifyAround: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
});


export default Globalstyles;