////////////////Native Imports 
import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'

///////////////installed Modules
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { DrawerActions } from '@react-navigation/native';



export function Header(props) {

    /////////////////this function would have been used for drawer toggle but i didn't make a drawer in this app so far
    const toggle = () => {
        props.rootProps.navigation.dispatch(DrawerActions.toggleDrawer())
    }

    return (
        <>
            <View style={styles.container}>
                <View style={styles.hamburgerIcon} >
                    <TouchableOpacity>
                        <FontAwesomeIcon icon={faBars} size={30} style={styles.icons} />
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({

    container: {
        flexDirection: "row",
        height: 50,
        justifyContent: "flex-end"
    },
    hamburgerIcon: {
        marginLeft: "2%",
        width: "12%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },

    icons: {
        color: "#fff"
    },
});
export default Header
