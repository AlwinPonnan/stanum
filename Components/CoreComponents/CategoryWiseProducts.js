////////////////Native Imports 
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground, TextInput, ScrollView, FlatList, ActivityIndicator } from 'react-native';

///////////////installed Modules
import LinearGradient from 'react-native-linear-gradient';
import { useIsFocused } from '@react-navigation/core';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

/////////////styles import
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';

//////////Component Imports
import Header from './Header';

///////////service imports
import { getCategory, getProducts } from '../api/services/Dealer';


export default function CategoryWiseProducts(props) {
    ////////////essentialVariables

    let focused = useIsFocused();

    /////////use states declaration

    const [categoryObj, setCategoryObj] = useState();
    const [productArr, setProductArr] = useState([]);
    const [categoryId, setCategoryId] = useState(props.route.params.data);


    useEffect(() => {
        if (focused) {
            getCategories();
            getProduct();
        }

        return () => {
            focused = false;
        }
    }, [focused])


    const getCategories = async () => {
        try {

            let res = await getCategory();
            if (res.success && focused) {
                let tempcategoryObj = res.data.find(el => el.id == categoryId)
                setCategoryObj(tempcategoryObj)
            }
        } catch (err) {
            console.log(err)
        }
    }

    const getProduct = async () => {
        try {
            let res = await getProducts();
            if (res.success && focused) {
                setProductArr(res.data);
            }
        } catch (err) {
            console.log(err)
        }
    }


    const renderProducts = ({ item, index }) => {
        return (
            <TouchableOpacity style={[GlobalStyles.flexRow, styles.cardContainer]} onPress={() => { props.navigation.navigate("ProductDetails", { data: item.id }) }}>
                <LinearGradient colors={['#FEC104', '#FE9D03', '#EE7D12']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} style={styles.linearGradient}>
                    {
                        item.pr_img[0] != '' && item.pr_img[0] != null
                            ?
                            ///////fallback image
                            <Image style={styles.cardImage} source={require('../Assets/Images/doubleCylinderFloorSpring.png')} />
                            :
                            <Image style={styles.cardImage} source={{ uri: item.pr_img[0] }} />
                    }
                    <Text style={styles.cardName}>{item.name}</Text>
                </LinearGradient>
            </TouchableOpacity>
        )
    }
    return (
        <View>
            {/* ///////////Head section/////////////// */}

            <ImageBackground style={{ ...GlobalStyles.gradientSection }} source={require('../Assets/Images/maingradientbg.png')}>
                <Header rootProps={props} />

                <Text style={styles.PageHeading}>
                    {categoryObj?.name}
                </Text>

            </ImageBackground>

            {/* ///////////////Body Section////////////////// */}

            <View style={{ backgroundColor: "white", borderTopLeftRadius: 40, borderTopRightRadius: 40, marginTop: -40, paddingTop: 50, zIndex: 1 }}>
                <TextInput style={{ ...styles.SearchInput, zIndex: 10 }} placeholder="Search" />
                <View style={{ height: hp(59) }}>
                    {
                        productArr && productArr.length > 0
                            ?
                            <FlatList
                                data={productArr}
                                renderItem={renderProducts}
                                keyExtractor={item => item.id}
                            />
                            :
                            <ActivityIndicator size="large" color="#FF9A00" />
                    }
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    ///////////////////////header section
    PageHeading: {
        color: "white",
        fontSize: 25,
        textAlign: "center",
        fontWeight: "bold",
        marginTop: hp(7)
    },


    //////////////////search input position setting
    SearchInput: {
        width: wp(90),
        shadowColor: "#000",
        backgroundColor: "white",
        paddingHorizontal: 20,
        borderColor: "transparent",
        borderWidth: 1,
        marginTop: -70,
        marginBottom: 20,
        zIndex: 2,
        borderRadius: 30,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,

        elevation: 5,
        alignSelf: "center"
    },

    /////////////card section
    cardContainer: {
        width: wp(85),
        height: 220,
        alignSelf: "center",
        justifyContent: "center",
        borderRadius: 20,
        marginVertical: 15,
        backgroundColor: "white",
        borderColor: "transparent",
        borderWidth: 1,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 0,
        elevation: 5,
    },
    linearGradient: {
        width: "100%",
        height: "100%",
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    cardImage: {
        width: "90%",
        height: 150,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    cardName: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#000",
        marginTop: 10
    },
})