////////////////Native Imports 
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground, TextInput, ScrollView, useWindowDimensions, ActivityIndicator } from 'react-native';

///////////////installed Modules
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SliderBox } from "react-native-image-slider-box";
import { useIsFocused } from '@react-navigation/core';
import HTML from "react-native-render-html";


/////////////styles import
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';


//////////Component Imports
import Header from './Header';


///////////service imports
import { getProductDetails, getProducts } from '../api/services/Dealer';

export default function ProductDetails(props) {
    ////////////essentialVariables
    const contentWidth = useWindowDimensions().width;
    const focused = useIsFocused();

    /////////use states declaration
    const [productDetailsObj, setProductDetailsObj] = useState();

    useEffect(() => {
        if (focused) {
            getProductDetail()
        }
    }, [focused])




    const getProductDetail = async () => {
        try {
            let res = await getProductDetails(props.route.params.data);
            if (res.success && focused) {
                setProductDetailsObj(res.data);
            }
        } catch (err) {
            console.log(err)
        }
    }

    return (
        <View>
            {/* ///////////Head section/////////////// */}

            <ImageBackground style={{ ...GlobalStyles.gradientSection, height: wp(30) }} source={require('../Assets/Images/maingradientbg.png')}>
                <Header rootProps={props} />
            </ImageBackground>

            {/* ///////////////Body Section////////////////// */}

            <View style={{ backgroundColor: "white", borderTopLeftRadius: 40, borderTopRightRadius: 40, marginTop: -40, paddingTop: 50, zIndex: 1 }}>
                <TextInput style={{ ...styles.SearchInput, zIndex: 10 }} placeholder="Search" />
                <ScrollView>
                    {
                        productDetailsObj
                            ?
                            <>
                                <SliderBox style={styles.ImageSlider} images={productDetailsObj?.pr_img} />
                                <Text style={styles.productCode}>{productDetailsObj?.p_code}</Text>
                                <Text style={styles.productName}>{productDetailsObj?.name}</Text>
                                <HTML source={{ html: productDetailsObj?.description }} contentWidth={contentWidth} containerStyle={{ marginLeft: 10, marginTop: 10, backgroundColor: "white" }} tagsStyles={{ li: { color: "black" }, p: { margin: 5, }, h3: { color: "#EA6918", fontSize: 25, marginLeft: 10 } }} />
                            </>
                            :
                            <ActivityIndicator size="large" color="#FF9A00" />

                    }
                </ScrollView>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    ///////////////////////header section
    PageHeading: {
        color: "white",
        fontSize: 25,
        textAlign: "center",
        fontWeight: "bold",
        marginTop: hp(7)
    },


    //////////////////search input position setting
    SearchInput: {
        width: wp(90),
        shadowColor: "#000",
        backgroundColor: "white",
        paddingHorizontal: 20,
        borderColor: "transparent",
        borderWidth: 1,
        marginTop: -70,
        marginBottom: 20,
        zIndex: 2,
        borderRadius: 30,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,

        elevation: 5,
        alignSelf: "center"
    },




    /////////////slider 
    ImageSlider: {
        height: 120,
        width: wp(90),
    },


    /////////////////product details Related sections
    productName: {
        fontWeight: "700",
        fontSize: 26,
        marginLeft: 10
    },
    productCode: {
        color: "#EA6918",
        fontWeight: "700",
        fontSize: 25,
        marginLeft: 10
    },

    /////////////card section
    cardContainer: {
        width: wp(85),
        height: 220,
        alignSelf: "center",
        justifyContent: "center",
        borderRadius: 20,
        marginVertical: 15,
        backgroundColor: "white",
        borderColor: "transparent",
        borderWidth: 1,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 0,
        elevation: 5,
    },
    linearGradient: {
        width: "100%",
        height: "100%",
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    cardImage: {
        width: "90%",
        height: 150,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    cardName: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#000",
        marginTop: 10
    },
})