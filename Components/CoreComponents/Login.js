////////////////Native Imports 
import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground, TextInput, ScrollView, ToastAndroid } from 'react-native'

///////////////installed Modules
import { useIsFocused } from '@react-navigation/core';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EncryptedStorage from 'react-native-encrypted-storage';

//////////Component Imports
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';

///////////service imports
import { login, decodeDealer_jwt } from '../api/services/Dealer';

export default function Login(props) {

    ////////////essentialVariables
    let focused = useIsFocused();


    /////////use states declaration

    ///////// form use states
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    ///check use State
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    ///////// use states declaration end here

    useEffect(() => {
        if (focused)
            LoginCheck()
        else
            focused = false;
    }, [focused])



    const LoginCheck = async () => {
        try {
            let Dealer = await decodeDealer_jwt();
            if (Dealer) {
                setIsLoggedIn(true)
                props.navigation.navigate("HomeScreen")
            }
            else {
                setIsLoggedIn(false)
            }

        } catch (err) {
            console.log(err)
        }
    }



    const HandleLogin = async () => {
        try {
            var DealerFormData = new FormData();

            DealerFormData.append('email', email);
            DealerFormData.append('password', password);

            let res = await login(DealerFormData);
            console.log(res)
            if (res.success) {
                // ToastAndroid.show(`${res.message}`, ToastAndroid.SHORT);
                await EncryptedStorage.setItem(
                    "dealer-token",
                    JSON.stringify({
                        token: res.data.token,
                        name: res.data.name
                    })
                );
                props.navigation.navigate("HomeScreen")

            }
            else {
                ToastAndroid.show(`${res.message}`, ToastAndroid.SHORT);
            }

        } catch (err) {
            console.log(err)
        }
    }

    return (
        <>
            <ScrollView>
                {/* ///////////Head section and logo///////////// */}

                <Image source={require('../Assets/Images/maingradientbg.png')} style={{
                    ...GlobalStyles.gradientSectionWithRadius, ...GlobalStyles.flexColumnJustifyCenter,
                }}>
                </Image>
                <Image source={require('../Assets/Images/logo.png')} style={{ ...GlobalStyles.logo }} />


                {/* ///////////Text inputs///////////// */}

                <View style={{ ...GlobalStyles.TextInputContainer, marginTop: 50 }}>

                    <Text style={GlobalStyles.textInputLabel}>Email Address</Text>

                    <TextInput onChangeText={(val) => setEmail(val)} style={{ ...GlobalStyles.textInputs }} />

                </View>
                <View style={{ ...GlobalStyles.TextInputContainer, marginBottom: 50 }}>

                    <Text style={GlobalStyles.textInputLabel}>Password</Text>

                    <TextInput onChangeText={(val) => setPassword(val)} style={{ ...GlobalStyles.textInputs }} secureTextEntry={true} />

                </View>


                {/* ///////////Buttons and Other Actions///////////// */}

                <TouchableOpacity style={[GlobalStyles.yellowButton, styles.loginBtn]} onPress={() => { HandleLogin() }}>
                    <Text style={GlobalStyles.buttonText}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { }}>
                    <Text style={{ alignSelf: "center" }}>Forgot password ?</Text>
                </TouchableOpacity>
                {
                    isLoggedIn == false &&
                    <TouchableOpacity onPress={() => { props.navigation.navigate("HomeScreen") }}>
                        <Text style={{ alignSelf: "center" }}>Press here to go directly to home page</Text>
                    </TouchableOpacity>

                }
                <View style={[GlobalStyles.flexRow, styles.signupContainer]}>

                    <Text>No Account?</Text>

                    <TouchableOpacity onPress={() => { props.navigation.navigate("Register") }}>
                        <Text style={GlobalStyles.buttonText}>SIGN UP</Text>
                    </TouchableOpacity>

                </View>

            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    loginBtn: {
        width: wp(25),
        marginBottom: 40
    },
    signupContainer: {
        width: wp(90),
        alignSelf: "center",
        justifyContent: "center",
        marginVertical: 15
    },
})