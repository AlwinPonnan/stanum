////////////////Native Imports 
import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet, TextInput, ScrollView, ToastAndroid } from 'react-native'


///////////////installed Modules
import { useIsFocused } from '@react-navigation/core';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { CheckBox } from 'react-native-elements';


//////////Component Imports
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';


///////////service imports
import { RegisterDealer } from '../api/services/Dealer'

export default function Register(props) {

    ////////////essentialVariables
    const focused = useIsFocused()

    /////////use states declaration

    ///check use State
    const [checked, setChecked] = useState(false);

    ////////form use states
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [name, setName] = useState();
    const [company, setCompany] = useState();
    const [phone, setPhone] = useState();
    const [confirm_password, setConfirm_password] = useState();



    const HandleRegister = async () => {
        try {
            var DealerFormData = new FormData();

            DealerFormData.append('email', email);
            DealerFormData.append('name', name);
            DealerFormData.append('company', company);
            DealerFormData.append('phone', phone);
            DealerFormData.append('password', password);
            DealerFormData.append('confirm_password', confirm_password);

            if (password == confirm_password) {
                let res = await RegisterDealer(DealerFormData);
                if (res.success) {
                    ToastAndroid.show(`${res.message}`, ToastAndroid.SHORT);
                    props.navigation.navigate("Login")
                }
                else
                    ToastAndroid.show(`${res.message}`, ToastAndroid.SHORT);
            }
            else
                ToastAndroid.show("Passwords Does not match", ToastAndroid.LONG);



        } catch (err) {
            console.log(err)
        }
    }

    return (
        <>
            <ScrollView>
                {/* ///////////Head section and logo///////////// */}

                <Image source={require('../Assets/Images/maingradientbg.png')} style={{
                    ...GlobalStyles.gradientSectionWithRadius, ...GlobalStyles.flexColumnJustifyCenter,
                }}>
                </Image>
                <Image source={require('../Assets/Images/logo.png')} style={{ ...GlobalStyles.logo }} />


                {/* ///////////Text inputs///////////// */}

                <View style={{ ...GlobalStyles.TextInputContainer, marginTop: 50 }}>

                    <Text style={GlobalStyles.textInputLabel}>Name</Text>

                    <TextInput onChangeText={(val) => setName(val)} style={{ ...GlobalStyles.textInputs }} />

                </View>
                <View style={{ ...GlobalStyles.TextInputContainer }}>

                    <Text style={GlobalStyles.textInputLabel}>Email Address</Text>

                    <TextInput onChangeText={(val) => setEmail(val)} keyboardType="email-address" style={{ ...GlobalStyles.textInputs }} />

                </View>

                <View style={{ ...GlobalStyles.TextInputContainer }}>

                    <Text style={GlobalStyles.textInputLabel}>Phone Number</Text>

                    <TextInput onChangeText={(val) => setPhone(val)} keyboardType="number-pad" style={{ ...GlobalStyles.textInputs }} />

                </View>

                <View style={{ ...GlobalStyles.TextInputContainer }}>

                    <Text style={GlobalStyles.textInputLabel}>Company Name</Text>

                    <TextInput onChangeText={(val) => setCompany(val)} style={{ ...GlobalStyles.textInputs }} />

                </View>
                <View style={{ ...GlobalStyles.TextInputContainer }}>

                    <Text style={GlobalStyles.textInputLabel}>Password</Text>

                    <TextInput onChangeText={(val) => setPassword(val)} style={{ ...GlobalStyles.textInputs }} secureTextEntry={true} />

                </View>

                <View style={{ ...GlobalStyles.TextInputContainer, marginBottom: 10 }}>

                    <Text style={GlobalStyles.textInputLabel}>Confirm Password</Text>

                    <TextInput onChangeText={(val) => setConfirm_password(val)} style={{ ...GlobalStyles.textInputs }} secureTextEntry={true} />

                </View>




                {/* ///////////Buttons and Other Actions///////////// */}

                <View style={[GlobalStyles.flexRowJustifyCenter, { alignItems: "center" }]}>

                    <CheckBox
                        containerStyle={{ width: wp(3) }}
                        center
                        checkedColor="#FF9303"
                        onIconPress={() => setChecked(!checked)}
                        checked={checked}
                    />

                    <Text>I agree to the </Text>

                    <TouchableOpacity>
                        <Text style={GlobalStyles.underlinedTxt}>Terms And Conditions</Text>
                    </TouchableOpacity>


                </View>



                <TouchableOpacity style={[GlobalStyles.yellowButton, styles.submitBtn]} onPress={() => { HandleRegister() }}>
                    <Text style={GlobalStyles.buttonText}>Submit</Text>
                </TouchableOpacity>

                <View style={[GlobalStyles.flexRow, styles.signupContainer]}>
                    <Text>Already have an Account?</Text>
                    <TouchableOpacity onPress={() => { props.navigation.navigate("Login") }}>
                        <Text style={GlobalStyles.buttonText}>SIGN IN</Text>
                    </TouchableOpacity>
                </View>


            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    submitBtn: {
        width: wp(35),
        marginBottom: 10
    },
    signupContainer: {
        width: wp(90),
        alignSelf: "center",
        justifyContent: "center",
        marginVertical: 15
    },
})