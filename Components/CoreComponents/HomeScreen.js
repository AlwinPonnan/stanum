////////////////Native Imports 
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground, TextInput, ScrollView, ToastAndroid } from 'react-native';

///////////////installed Modules
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EncryptedStorage from 'react-native-encrypted-storage';
import { useIsFocused } from '@react-navigation/core';

//////////Component Imports
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';
import Header from './Header';

///////////service imports
import { decodeDealer_jwt } from '../api/services/Dealer';


export default function HomeScreen(props) {
    ////////////essentialVariables
    let focused = useIsFocused();


    /////////use states declaration
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [dealerName, setDealerName] = useState();

    useEffect(() => {
        if (focused)
            LoginCheck()
        else
            focused = false;
    }, [focused])

    const LoginCheck = async () => {
        try {
            let Dealer = await decodeDealer_jwt();
            if (Dealer) {
                setIsLoggedIn(true)
                const Dealertoken = await EncryptedStorage.getItem("dealer-token");;
                let tempName = JSON.parse(Dealertoken).name;
                setDealerName(tempName.charAt(0));
            }
            else
                ToastAndroid.show(`No user Logged in !!`, ToastAndroid.SHORT);
        } catch (err) {
            console.log(err)
        }
    }
    return (
        <View>
            {/* ///////////Head section/////////////// */}

            <ImageBackground style={GlobalStyles.gradientSection} source={require('../Assets/Images/maingradientbg.png')}>
                <Header rootProps={props} />

                {
                    isLoggedIn ?
                        <>
                            <View style={styles.UserNameCircle}>
                                <Text style={styles.UserName}>{dealerName}</Text>
                            </View>

                            <Text style={styles.userWelcome}>Welcome Dealer!</Text>


                        </>
                        :
                        <View style={GlobalStyles.flexColumnJustifyCenter}>
                            <TouchableOpacity style={[GlobalStyles.whiteButton, styles.WhiteBtnWidth, { marginTop: 20 }]} onPress={() => props.navigation.navigate("Login")}><Text style={GlobalStyles.whiteButtonText}>Log In</Text></TouchableOpacity>
                            <Text style={styles.headerTxt}>Or</Text>
                            <TouchableOpacity style={[GlobalStyles.whiteButton, styles.WhiteBtnWidth, { marginTop: 10 }]} onPress={() => props.navigation.navigate("Register")}><Text style={GlobalStyles.whiteButtonText}>Register</Text></TouchableOpacity>
                        </View>
                }

                <TextInput style={GlobalStyles.SearchInput} placeholder="Search" />
            </ImageBackground>

            {/* ///////////////Body Section////////////////// */}

            <View style={{ marginTop: 80, }}>
                <TouchableOpacity style={[GlobalStyles.flexRow, styles.cardContainer]} onPress={() => { props.navigation.navigate("ProductCategories") }}>
                    <Image style={styles.cardImage} source={require('../Assets/Images/dropBox.png')} />
                    <View style={styles.cardNameContainer}>
                        <Text style={styles.cardName}>Catalogue</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={[GlobalStyles.flexRow, styles.cardContainer]}>
                    <Image style={styles.cardImage} source={require('../Assets/Images/setting.png')} />
                    <View style={styles.cardNameContainer}>
                        <Text style={styles.cardName}>Service Request</Text>
                    </View>
                </TouchableOpacity>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    ///////////////////////header section
    WhiteBtnWidth: {
        width: wp(60),
        alignSelf: "center"
    },
    headerTxt: {
        color: "white",
        alignSelf: "center",
        textAlign: "center",
        marginTop: 10,
        fontSize: 20
    },
    UserNameCircle: {
        width: 100,
        height: 100,
        backgroundColor: "white",
        borderRadius: 150,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    UserName: {
        fontSize: 60,
        color: "#F8D253",
    },
    userWelcome: {
        color: "white",
        alignSelf: "center",
        textAlign: "center",
        marginTop: 10,
        fontSize: 25,
    },




    /////////////card section
    cardContainer: {
        width: wp(85),
        alignSelf: "center",
        alignItems: "center",
        borderRadius: 10,
        padding: 10,
        marginVertical: 15,
        backgroundColor: "white",
        borderColor: "transparent",
        borderWidth: 1,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 0,

        elevation: 5,
    },
    cardImage: {
        height: 50,
        width: 60,
    },
    cardNameContainer: {
        alignSelf: "center",
        flex: 1,
        alignItems: "center",
        textAlign: "center",
        borderWidth: 2,
        marginLeft: 15,
        borderTopColor: "transparent",
        borderLeftColor: "grey",
        borderBottomColor: "transparent",
        borderRightColor: "transparent",

    },
    cardName: {
        fontSize: 27,
        color: "#434343",
    },
})