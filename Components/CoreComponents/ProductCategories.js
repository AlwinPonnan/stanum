////////////////Native Imports 
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground, TextInput, ScrollView, FlatList, ToastAndroid, ActivityIndicator } from 'react-native';

///////////////installed Modules
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useIsFocused } from '@react-navigation/core';

/////////////styles import
import GlobalStyles from '../Assets/GlobalStyles/GlobalStyles';


//////////Component Imports 
import Header from './Header';

///////////service imports
import { getCategory } from '../api/services/Dealer';


export default function ProductCategories(props) {

    ////////////essentialVariables
    let focused = useIsFocused();

    /////////use states declaration
    const [CategoryArr, setCategoryArr] = useState([]);


    useEffect(() => {
        if (focused) {
            getCategories()
        }

        return () => {
            focused = false;
        }

    }, [focused])


    const getCategories = async () => {
        try {

            let res = await getCategory();
            if (res.success && focused) {
                setCategoryArr(res.data);
            }
            else
                ToastAndroid.show(`${res.message}`, ToastAndroid.SHORT);
        } catch (err) {
            console.log(err)
        }
    }


    const renderCategories = ({ item, index }) => {
        return (
            <TouchableOpacity style={[GlobalStyles.flexRow, styles.cardContainer]} onPress={() => { props.navigation.navigate("CategoryWiseProducts", { data: item.id }) }}>
                {
                    item.image != '' && item.image != null
                        ?
                        <Image style={styles.cardImage} source={{ uri: item?.image }} />
                        :
                        <Image style={styles.cardImage} source={require('../Assets/Images/showeraccesories.png')} />
                }
                <View style={styles.cardNameContainer}>
                    <Text style={styles.cardName}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    return (
        <View>

            {/* ///////////Head section/////////////// */}
            <ImageBackground style={{ ...GlobalStyles.gradientSection }} source={require('../Assets/Images/maingradientbg.png')}>
                <Header rootProps={props} />

                <Text style={styles.PageHeading}>
                    Product Categories
                </Text>

            </ImageBackground>
            {/* ///////////////Body Section////////////////// */}
            <View style={{ backgroundColor: "white", borderTopLeftRadius: 40, borderTopRightRadius: 40, marginTop: -40, paddingTop: 50, zIndex: 1 }}>
                <TextInput style={{ ...styles.SearchInput, zIndex: 10 }} placeholder="Search" />
                <View style={{ height: hp(59) }}>
                    {
                        CategoryArr && CategoryArr.length > 0
                            ?
                            <FlatList
                                data={CategoryArr}
                                renderItem={renderCategories}
                                keyExtractor={item => item.id}
                            />
                            :
                            <ActivityIndicator size="large" color="#FF9A00" />
                    }

                </View>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    ///////////////////////header section
    PageHeading: {
        color: "white",
        fontSize: 25,
        textAlign: "center",
        fontWeight: "bold",
        marginTop: hp(7)
    },


    //////////////////search input position setting
    SearchInput: {
        width: wp(90),
        shadowColor: "#000",
        backgroundColor: "white",
        paddingHorizontal: 20,
        borderColor: "transparent",
        borderWidth: 1,
        marginTop: -70,
        marginBottom: 20,
        zIndex: 2,
        borderRadius: 30,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,

        elevation: 5,
        alignSelf: "center"
    },

    /////////////card section
    cardContainer: {
        width: wp(85),
        alignSelf: "center",
        alignItems: "center",
        borderRadius: 10,
        padding: 10,
        marginVertical: 15,
        backgroundColor: "white",
        borderColor: "transparent",
        borderWidth: 1,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 0,

        elevation: 5,
    },
    cardImage: {
        height: 50,
        width: 60,
    },
    cardNameContainer: {
        alignSelf: "center",
        flex: 1,
        alignItems: "center",
        textAlign: "center",
        borderWidth: 2,
        marginLeft: 15,
        borderTopColor: "transparent",
        borderLeftColor: "grey",
        borderBottomColor: "transparent",
        borderRightColor: "transparent",

    },
    cardName: {
        fontSize: 27,
        color: "#434343",
    },
})