////////////////Native Imports 
import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';


///////////////installed Modules
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SplashScreen from 'react-native-splash-screen'


//////////Component Imports
import Login from './Components/CoreComponents/Login';
import Register from './Components/CoreComponents/Register';
import HomeScreen from './Components/CoreComponents/HomeScreen';
import ProductCategories from './Components/CoreComponents/ProductCategories';
import CategoryWiseProducts from './Components/CoreComponents/CategoryWiseProducts';
import ProductDetails from './Components/CoreComponents/ProductDetails';


///////////service imports
import { decodeDealer_jwt } from './Components/api/services/Dealer';

const App = () => {

  /////////use states declaration
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  ////////////essentialVariables
  const Stack = createStackNavigator();



  useEffect(() => {
    LoginCheck()
  }, [])


  const LoginCheck = async () => {
    try {
      let Dealer = await decodeDealer_jwt();
      if (Dealer) {
        setIsLoggedIn(true);
        SplashScreen.hide();
      }
      else {
        setIsLoggedIn(false)
        SplashScreen.hide();

      }

    } catch (err) {
      console.log(err)
    }
  }






  return (
    <>
      <SafeAreaProvider>
        <NavigationContainer>
          <Stack.Navigator headerMode="none">
            {/* /////////////// this code is here to check whether the user is logged in and switch the navigations based on that ////////////////// */}
            {
              isLoggedIn ?
                <>
                  <Stack.Screen name="HomeScreen" component={HomeScreen} />
                  <Stack.Screen name="ProductCategories" component={ProductCategories} />
                  <Stack.Screen name="CategoryWiseProducts" component={CategoryWiseProducts} />
                  <Stack.Screen name="ProductDetails" component={ProductDetails} />
                </>
                :
                <>
                  <Stack.Screen name="Login" component={Login} />
                  <Stack.Screen name="Register" component={Register} />
                  <Stack.Screen name="HomeScreen" component={HomeScreen} />
                  <Stack.Screen name="ProductCategories" component={ProductCategories} />
                  <Stack.Screen name="CategoryWiseProducts" component={CategoryWiseProducts} />
                  <Stack.Screen name="ProductDetails" component={ProductDetails} />
                </>
            }
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </>
  );
};


export default App;
