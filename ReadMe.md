Folder Structure :-

1. Components Folder has all the files needed basically it is the main folder
2. Api folder Contains services
3. Assets folder contains globalStyles folder which contains global styles file and images folder which contains all the images .

4. GlobalStyles file has all the styles which is required to be globally available.
5. Core Components folder has all the Components which contain the actual layout and design which has to be rendered on user's machine.
6. Navigators folder will contain all the navigators which help in organising the navigators while nesting

App flow :-

1. index.js
2. app.js
3. after app.js based on the login status user will be navigated to either of the 2 navigation options which is a stack Navigator
